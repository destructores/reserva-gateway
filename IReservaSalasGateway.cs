﻿using ReservaGateway.SalasProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ReservaGateway
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReservaSalasGateway" in both code and config file together.
    [ServiceContract]
    public interface IReservaSalasGateway
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Usuarios/{dni}", ResponseFormat = WebMessageFormat.Json)]
        UsuariosProxy.Usuario ObtenerUsuario(string dni);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Usuarios", ResponseFormat = WebMessageFormat.Json)]
        List<ReservaGateway.UsuariosProxy.Usuario> ListarUsuarios();


        [OperationContract]
        [FaultContract(typeof(SalaException))]
        [WebInvoke(Method = "GET", UriTemplate = "Salas/{zona}", ResponseFormat = WebMessageFormat.Json)]
        List<ReservaGateway.SalasProxy.Sala> ObtenerSala(string zona);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Salas", ResponseFormat = WebMessageFormat.Json)]
        List<ReservaGateway.SalasProxy.Sala> ListarSalas();
    }
}
