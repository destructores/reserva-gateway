﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ReservaGateway.SalasProxy;
using System.ServiceModel.Web;
using System.Net;

namespace ReservaGateway
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReservaSalasGateway" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ReservaSalasGateway.svc or ReservaSalasGateway.svc.cs at the Solution Explorer and start debugging.
    public class ReservaSalasGateway : IReservaSalasGateway
    {


        public List<ReservaGateway.UsuariosProxy.Usuario> ListarUsuarios()
        {
            UsuariosProxy.UsuariosServiceClient proxy = new UsuariosProxy.UsuariosServiceClient();
            var listaUsuarios = proxy.ListarUsuarios();
            int tamanio = 0;
            List<ReservaGateway.UsuariosProxy.Usuario> listaFinal = new List<ReservaGateway.UsuariosProxy.Usuario>();
            
            while (tamanio < listaUsuarios.Length)
            {

               // listaFinal.Add("aaron paz" + listaUsuarios[tamanio].ToString());

                ReservaGateway.UsuariosProxy.Usuario usuarios = (ReservaGateway.UsuariosProxy.Usuario) listaUsuarios[tamanio];
               listaFinal.Add(usuarios);
                tamanio = tamanio + 1;
            }

            return listaFinal;
        }

        public List<ReservaGateway.SalasProxy.Sala> ObtenerSala(string identificadorZona)
        {
            List<ReservaGateway.SalasProxy.Sala> listaFinal = new List<ReservaGateway.SalasProxy.Sala>();
            try
            {
                SalasProxy.SalasServiceClient proxy = new SalasProxy.SalasServiceClient();

                var listaSalas = proxy.ObtenerSala(identificadorZona);
                int tamanio = 0;
                

                while (tamanio < listaSalas.Length)
                {
                    // listaFinal.Add("aaron paz" + listaUsuarios[tamanio].ToString());
                    ReservaGateway.SalasProxy.Sala salas = (ReservaGateway.SalasProxy.Sala)listaSalas[tamanio];
                    listaFinal.Add(salas);
                    tamanio = tamanio + 1;
                }
            } catch (Exception e)
            {
                throw new WebFaultException<string>(
                    e.Message , HttpStatusCode.Found
                );
            }
            return listaFinal;
        }

        public List<ReservaGateway.SalasProxy.Sala> ListarSalas()
        {
            SalasProxy.SalasServiceClient proxy = new SalasProxy.SalasServiceClient();

            var listaSalas = proxy.ListarSalas();
            int tamanio = 0;
            List<ReservaGateway.SalasProxy.Sala> listaFinal = new List<ReservaGateway.SalasProxy.Sala>();

            while (tamanio < listaSalas.Length)
            {
                ReservaGateway.SalasProxy.Sala salas = (ReservaGateway.SalasProxy.Sala)listaSalas[tamanio];
                listaFinal.Add(salas);
                tamanio = tamanio + 1;
            }

            return listaFinal;
        }

        public UsuariosProxy.Usuario ObtenerUsuario(string dni)
        {
            try
            {
                UsuariosProxy.UsuariosServiceClient proxy = new UsuariosProxy.UsuariosServiceClient();
                return proxy.ObtenerUsuario(int.Parse(dni));
            }
            catch (Exception e)
            {
                throw new WebFaultException<string>(e.Message, HttpStatusCode.Found);
            }
        }
    }
}
